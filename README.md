# PoC for GitLab Flavored Markdown Reference Title

## Issue

- #1
- #1+

## MR

- !1
- !1+

## Markdown Source of the Above

```markdown
# PoC for GitLab Flavored Markdown Reference Title

## Issue

- #1
- #1+

## MR

- !1
- !1+
```

## Problem: Rendering of titles is not consistent!

### Web IDE and issue/MR markdown

![](./markdown_preview_web_ide.png)

**Working as expected**

### File Editor and Repo Browser markdown

![](./markdown_preview_file_editor.png)

![](./markdown_rendered_repo_browser.png)
